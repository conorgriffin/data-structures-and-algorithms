package com.conorgriffin.dsa.assignment3;

@SuppressWarnings("serial")
public class EmptyStackException extends RuntimeException {
	public EmptyStackException (String err) {
		super(err);
	}
}
