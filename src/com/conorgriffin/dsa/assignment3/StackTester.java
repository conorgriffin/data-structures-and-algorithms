package com.conorgriffin.dsa.assignment3;

public class StackTester {
	public static void main(String[] args) {
		ArrayStack<Character> as = new ArrayStack<Character>();

		as.push('e');
		System.out.println(as);
		as.push('s');
		System.out.println(as);
		as.push('c');
		System.out.println(as);
		as.pop();
		System.out.println(as);
		as.push('u');
		System.out.println(as);
		as.push('a');
		System.out.println(as);
		as.pop();
		System.out.println(as);
		as.push('o');
		System.out.println(as);
		as.push('t');
		System.out.println(as);
		as.pop();
		System.out.println(as);
		as.push('h');
		System.out.println(as);
		
		LinkedStack<Character> ls = new LinkedStack<Character>();

		ls.push('e');
		System.out.println(ls);
		ls.push('s');
		System.out.println(ls);
		ls.push('c');
		System.out.println(ls);
		ls.pop();
		System.out.println(ls);
		ls.push('u');
		System.out.println(ls);
		ls.push('a');
		System.out.println(ls);
		ls.pop();
		System.out.println(ls);
		ls.push('o');
		System.out.println(ls);
		ls.push('t');
		System.out.println(ls);
		ls.pop();
		System.out.println(ls);
		ls.push('h');		
		System.out.println(ls);
	}
}
