package com.conorgriffin.dsa.assignment3;

public class PCQ3A {
	public static void main(String[] args) {
		ArrayStack<String> as = new ArrayStack<String>();
		
		System.out.println(as);
		as.push("Ireland");
		System.out.println(as);
		as.pop();
		System.out.println(as);
		as.push("England");
		System.out.println(as);
		as.pop();
		System.out.println(as);
		as.push("Wales");
		System.out.println(as);
		as.pop();
		System.out.println(as);
		as.push("Scotland");
		System.out.println(as);
		as.pop();
		System.out.println(as);
		as.push("France");
		System.out.println(as);
		as.push("Germany");
		System.out.println(as);
	}
}
