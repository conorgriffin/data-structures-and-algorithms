package com.conorgriffin.dsa.assignment3;

public class PCQ2A {
	public static void main(String[] args) {
		ArrayStack<Character> as = new ArrayStack<Character>();

		System.out.println(as);
		as.push('e');
		System.out.println(as);
		as.push('s');
		System.out.println(as);
		as.push('c');
		System.out.println(as);
		as.pop();
		System.out.println(as);
		as.push('u');
		System.out.println(as);
		as.push('a');
		System.out.println(as);
		as.pop();
		System.out.println(as);
		as.push('o');
		System.out.println(as);
		as.push('t');
		System.out.println(as);
		as.pop();
		System.out.println(as);
		as.push('h');
		System.out.println(as);
	}
}
