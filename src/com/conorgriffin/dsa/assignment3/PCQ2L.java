package com.conorgriffin.dsa.assignment3;

public class PCQ2L {
	public static void main(String[] args) {
		LinkedStack<Character> ls = new LinkedStack<Character>();

		System.out.println(ls);
		ls.push('e');
		System.out.println(ls);
		ls.push('s');
		System.out.println(ls);
		ls.push('c');
		System.out.println(ls);
		ls.pop();
		System.out.println(ls);
		ls.push('u');
		System.out.println(ls);
		ls.push('a');
		System.out.println(ls);
		ls.pop();
		System.out.println(ls);
		ls.push('o');
		System.out.println(ls);
		ls.push('t');
		System.out.println(ls);
		ls.pop();
		System.out.println(ls);
		ls.push('h');		
		System.out.println(ls);
	}
}
