package com.conorgriffin.dsa.assignment3;

public class PCQ3L {
	public static void main(String[] args) {
		LinkedStack<String> ls = new LinkedStack<String>();
		
		System.out.println(ls);
		ls.push("Ireland");
		System.out.println(ls);
		ls.pop();
		System.out.println(ls);
		ls.push("England");
		System.out.println(ls);
		ls.pop();
		System.out.println(ls);
		ls.push("Wales");
		System.out.println(ls);
		ls.pop();
		System.out.println(ls);
		ls.push("Scotland");
		System.out.println(ls);
		ls.pop();
		System.out.println(ls);
		ls.push("France");
		System.out.println(ls);
		ls.push("Germany");
		System.out.println(ls);
	}
}
