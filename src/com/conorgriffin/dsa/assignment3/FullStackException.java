package com.conorgriffin.dsa.assignment3;

@SuppressWarnings("serial")
public class FullStackException extends RuntimeException {
	public FullStackException (String err) {
		super(err);
	}
}
