package com.conorgriffin.dsa.assignment3;

/**
 * A generic stack interface that provides the standard 5 methods for a stack;
 * push(), pop(), top(), isEmpty() and size()
 * @author conorgriffin
 *
 */
public interface Stack<E> {
	/**
	 * Return the number of elements in the stack
	 * @return number of elements in the stack
	 */
	public int size();
	
	/**
	 * Return whether the stack is empty
	 * @return true if the stack is empty, false otherwise
	 */
	public boolean isEmpty();
	
	/**
	 * Insert an element at the top of the stack
	 * @param element to be inserted
	 */
	public void push(E element);
	
	/**
	 * Remove an element from the top of the stack
	 * @return the element removed
	 * @throws EmptyStackException
	 */
	public E pop() throws EmptyStackException;
	
	/**
	 * Inspect the element at the top of the stack
	 * @return top element in the stack
	 * @throws EmptyStackException
	 */
	public E top() throws EmptyStackException;
	
}
