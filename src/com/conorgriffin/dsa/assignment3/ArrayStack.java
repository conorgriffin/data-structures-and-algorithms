package com.conorgriffin.dsa.assignment3;

/**
 * An implementation of an array-based stack
 * 
 * @author conorgriffin
 *
 */
public class ArrayStack<E> implements Stack<E> {
	
	private E S[];
	private int size;
	private int top = -1;
	
	@SuppressWarnings("unchecked")
	public ArrayStack(int capacity) {
		size = capacity;
		S = (E[]) new Object[size];
	}
	
	public ArrayStack() {
		this(1000);
	}

	@Override
	public int size() {
		return top + 1;
	}

	@Override
	public boolean isEmpty() {
		return top == -1 ? true : false;
	}

	@Override
	public void push(E element) throws FullStackException {
		if(top == S.length -1) throw new FullStackException("The stack is full, cannot push " + element.toString());
		top = top + 1;
		S[top] = (E) element;
	}

	@Override
	public E pop() throws EmptyStackException {
		if (isEmpty()) throw new EmptyStackException("Empty stack, cannot pop");
		E temp = S[top];
		S[top] = null;
		top = top - 1;
		return temp;
	}

	@Override
	public E top() throws EmptyStackException {
		if (isEmpty()) throw new EmptyStackException("Empty stack, no objects found");
		return S[top];
	}

	/**
	 * Print out a string representation of the stack
	 * @return a string with the count of objects followed by each element separated by spaces
	 */
	public String toString() {

		String output = "";
		for(int i = top; i > -1; i--) {
			output = S[i].toString() + " " + output;
		}
		
		output = top + 1 + "\t" + output;

		return output;
	}
	
	public static void main(String[] args) {
		ArrayStack<String> s = new ArrayStack<String>();
		System.out.println(s);
		s.push("A");
		System.out.println(s);
		s.push("B");
		System.out.println(s);
		s.push("C");
		System.out.println(s);
	}
}
