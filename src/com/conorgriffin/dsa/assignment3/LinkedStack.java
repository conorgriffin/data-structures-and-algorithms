package com.conorgriffin.dsa.assignment3;

/**
 * An implementation of a linked-list-based stack
 * 
 * @author conorgriffin
 *
 */
public class LinkedStack<E> implements Stack<E> {
	
	protected Node top;
	private int size;

	protected class Node {
		E element;
		Node next;
		
		public Node(E element) {
			this.element = element;
		}
		
		public String toString() {
			return element.toString();
		}
	}
	
	public LinkedStack() {
		top = null;
		size = 0;
	}
	
	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0 ? true : false;
	}

	@Override
	public void push(E o) {
		Node node = new Node(o);
		node.next = top;
		top = node;
		size++;
	}

	@SuppressWarnings("unchecked")
	@Override
	public E pop() throws EmptyStackException {
		if(size == 0) throw new EmptyStackException("Empty stack, cannot pop");
		Node node = top;
		top = node.next;
		size--;
		return (E) top;
	}

	@SuppressWarnings("unchecked")
	@Override
	public E top() throws EmptyStackException {
		if(size == 0) throw new EmptyStackException("Empty stack, no objects found");
		return (E) top;
	}
	
	public String toString() {
		String output = "";
		Node node = top;
		while (node != null) {
			output = node.element.toString() + " " + output;
			node = node.next;
		}
		
		return size + "\t" + output;
	}

	public static void main(String[] args) {
		LinkedStack<String> s = new LinkedStack<String>();
		System.out.println(s);
		s.push("A");
		System.out.println(s);
		s.push("B");
		System.out.println(s);
		s.push("C");
		System.out.println(s);
	}
}
