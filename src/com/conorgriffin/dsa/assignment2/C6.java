package com.conorgriffin.dsa.assignment2;

public class C6 extends AlgorithmTimer {
	
	public static void main(String[] args) {	
		C6 c6 = new C6();
		c6.start();
	}
	
	private static int minValueIndex(int[] a, int n) {
		int minValueIndex = 0;
		
		for(int k = 1; k < n; k++) {
			if(a[minValueIndex] > a[k]) {
				minValueIndex = k;
			}
		}
		return minValueIndex;
	}

	@Override
	protected void runAlgorithm(int[] testArray) {
		minValueIndex(testArray, testArray.length);
	}
}
