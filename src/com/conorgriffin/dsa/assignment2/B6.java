package com.conorgriffin.dsa.assignment2;

public class B6 {
	private static int[] anArray = new int[] {1, 4, 6, 9, 0};
	
	public static void main(String[] args) {	
		System.out.println("output");
		System.out.println(minValueIndex(anArray, anArray.length));
	}
	
	private static int minValueIndex(int[] a, int n) {
		int minValueIndex = 0;
		
		for(int k = 1; k < n; k++) {
			if(a[minValueIndex] > a[k]) {
				minValueIndex = k;
			}
		}
		return minValueIndex;
	}
}
