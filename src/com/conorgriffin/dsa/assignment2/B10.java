package com.conorgriffin.dsa.assignment2;

public class B10 {
	
	private static int[] anArray = new int[] {1,2,3,4,5};
	
	public static void main(String[] args) {
		System.out.println("output");
		System.out.println(linearSearch(anArray, anArray.length, 4));
	}
	
	private static int linearSearch(int[] anArray, int n, int q) {
		int index = 0;
		
		while(index < n && anArray[index] != q) {
			index = index + 1;
		}
		
		if(index == anArray.length)
			return -1;
		else
			return index;
	}
}
