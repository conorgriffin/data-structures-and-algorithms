package com.conorgriffin.dsa.assignment2;

public class B4 {
	public static void main(String[] args) {
		System.out.println("output");
		System.out.println(difference(5, 4));
	}

	private static int difference(int a, int b) {
		if(a > b)
			return a - b;
		else
			return b - a;
	}
}
