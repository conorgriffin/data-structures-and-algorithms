package com.conorgriffin.dsa.assignment2;

import java.util.Random;

public abstract class AlgorithmTimer {
	
	protected long startTime;
	protected long endTime;
	private static long numberOfRuns = 5L;
	
	private static final int[] testValues = new int[] {
		10000000, 
		20000000, 
		30000000, 
		40000000, 
		50000000, 
		60000000, 
		70000000, 
		80000000, 
		90000000, 
		100000000
		};

	protected void start() {
		
		Random generator = new Random();
		long timing1 = 0;
		
		// loop through testValues to create new arrays of specific sizes
		for(int i = 0; i < testValues.length; i++) {
			int[] testArray = new int[testValues[i]];
			
			// do 5 runs to account for system CPU scheduling/priority
			for (int k = 0; k < numberOfRuns; k++) {
				
				// loop over each array of random numbers
				for (int j = 0; j < testArray.length; j++) {
					testArray[j] = generator.nextInt();
				}
				startTime = System.currentTimeMillis();
				runAlgorithm(testArray);
				endTime = System.currentTimeMillis();
				timing1 += endTime - startTime;
			}
			
			// averaging the timing over 5 runs
			timing1 = timing1 / numberOfRuns;
			System.out.println(testArray.length + ", " + timing1);
		}
	}
	
	protected abstract void runAlgorithm(int[] testArray);
}
