package com.conorgriffin.dsa.assignment2;

public class C4 extends AlgorithmTimer {
	public static void main(String[] args) {
		C4 c4 = new C4();
		c4.start();
	}

	private static int difference(int a, int b) {
		if(a > b)
			return a - b;
		else
			return b - a;
	}

	@Override
	protected void runAlgorithm(int[] testArray) {
		difference(5, 4);
	}
}
