package com.conorgriffin.dsa.assignment2;

public class C10 extends AlgorithmTimer {
		
	public static void main(String[] args) {
		C10 c10 = new C10();
		c10.start();
	}
	
	private static int linearSearch(int[] anArray, int n, int q) {
		int index = 0;
		
		while(index < n && anArray[index] != q) {
			index = index + 1;
		}
		
		if(index == anArray.length)
			return -1;
		else
			return index;
	}

	@Override
	protected void runAlgorithm(int[] testArray) {
		linearSearch(testArray, testArray.length, 5000);
	}
}
