package com.conorgriffin.dsa.assignment4;

@SuppressWarnings("serial")
public class EmptyQueueException extends RuntimeException {
	public EmptyQueueException (String err) {
		super(err);
	}
}
