package com.conorgriffin.dsa.assignment4;

/**
 * An implementation of a double-ended queue (deque)
 * 
 * @author conorgriffin
 */
public class LinkedDeque<E> implements Deque<E> {
	
	DLNode<E> front, rear;
	int size = 0;

	public LinkedDeque() {
		front = new DLNode<E>();
		rear = new DLNode<E>();
		front.setNext(rear);
		rear.setPrev(front);
	}
	
	@Override
	public void insertFirst(E element) {
	    DLNode<E> second = front.getNext();
	    DLNode<E> first = new DLNode<E>(element, front, second);
	    second.setPrev(first);
	    front.setNext(first);
	    size++;		
	}

	@Override
	public void insertLast(E element) {
	    DLNode<E> secondLast = rear.getPrev();
	    DLNode<E> last = new DLNode<E>(element, secondLast, rear);
	    secondLast.setNext(last);
	    rear.setPrev(last);
	    size++;		
	}

	@Override
	public E removeFirst() {
		if(isEmpty()) throw new EmptyDequeException("The deque is empty, cannot remove the first element");
		DLNode<E> first = front.getNext();
		E o = first.getElement();
		DLNode<E> second = first.getNext();
		front.setNext(second);
		second.setPrev(front);
		size--;
		return o;
	}

	@Override
	public E removeLast() {
		if(isEmpty()) throw new EmptyDequeException("The deque is empty, cannot remove the last element");
		DLNode<E> last = rear.getPrev();
		E o = last.getElement();
		DLNode<E> secondLast = last.getPrev();
		rear.setPrev(secondLast);
		secondLast.setNext(rear);
		size--;
		return o;
	}

	@Override
	public E getFirst() {
	    if (isEmpty()) throw new EmptyDequeException("Deque is empty. Nothing to fetch");
	      return front.getNext().getElement();
	}

	@Override
	public E getLast() {
	    if (isEmpty()) throw new EmptyDequeException("Deque is empty. Nothing to fetch");
	      return rear.getPrev().getElement();
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
	    if (size == 0) {
	        return true;
	    }
	    return false;
	}
	
	public String toString() {
		String output = size + "\t";
		DLNode<E> node = front;
		for(int i = 1; i <= size; i++) {
			output += node.getNext().getElement().toString() + " ";
			node = node.getNext();
		}
		return output;
	}

	public static void main(String[] args) {
		LinkedDeque<String> l = new LinkedDeque<String>();
		System.out.println(l);
		l.insertLast("A");
		System.out.println(l);
		l.insertLast("B");
		System.out.println(l);
		l.insertLast("C");
		System.out.println(l);
	}
}