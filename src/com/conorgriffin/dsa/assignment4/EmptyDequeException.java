package com.conorgriffin.dsa.assignment4;

@SuppressWarnings("serial")
public class EmptyDequeException extends RuntimeException {
	public EmptyDequeException (String err) {
		super(err);
	}
}
