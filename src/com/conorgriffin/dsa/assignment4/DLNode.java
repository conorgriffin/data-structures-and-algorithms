package com.conorgriffin.dsa.assignment4;

/**
 * An abstract data type to represent an item in a double linked list
 * 
 * @param <E>
 */
public class DLNode<E> {
  private E element;
  private DLNode<E> next, prev;
  
  DLNode() {
	  this(null, null, null);
  }
  
  DLNode(E e, DLNode<E> previous, DLNode<E> next) {
	  element = e;
	  this.next = next;
	  this.prev = previous;
  }
  
  public void setElement(E newElem) {
	  element = newElem;
  }
  
  public void setNext(DLNode<E> newNext) {
	  next = newNext;
  }
  
  public void setPrev(DLNode<E> newPrev) {
	  prev = newPrev;
  }
  
  public E getElement() {
	  return element;
  }
  
  public DLNode<E> getNext() {
	  return next;
  }
  
  public DLNode<E> getPrev() {
	  return prev;
  }
  
}
