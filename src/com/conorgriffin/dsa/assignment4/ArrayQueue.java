package com.conorgriffin.dsa.assignment4;

public class ArrayQueue<E> implements Queue<E> {
	
	private int N = 1000;
	private E Q[];
	private int front = 0;
	private int rear = 0;
	
	@SuppressWarnings("unchecked")
	public ArrayQueue(int capacity) {
		N = capacity;
		Q = (E[]) new Object[N];
	}
	
	@SuppressWarnings("unchecked")
	public ArrayQueue() {
		Q = (E[]) new Object[N];
	}

	@Override
	public void enqueue(E element) throws FullQueueException {
		if((rear + 1) % N == front) throw new FullQueueException("Queue is full, cannot enqueue " + element.toString());
		Q[rear] = element;
		rear = (rear + 1) % N;
	}

	@Override
	public E dequeue() throws EmptyQueueException {
		if(rear == front) throw new EmptyQueueException("The queue is empty, nothing to dequeue");
		E first = Q[front];
		Q[front] = null;
		front = (front + 1) % N;
		return first;
	}

	@Override
	public E front() throws EmptyQueueException {
		if(rear == front) throw new EmptyQueueException("The queue is empty, nothing to fetch");
		return Q[front];
	}

	@Override
	public boolean isEmpty() {
		return rear == front;
	}

	@Override
	public int size() {
		return (N + rear - front) % N;
	}
	
	public String toString() {
		String output = "";
		for(int i = 0; i < size(); i++) {
			output += Q[(front + i) % N].toString() + " ";
		}
		output = size() + "\t" + output;
		return output;
	}

	public static void main(String[] args) {
		ArrayQueue<String> q = new ArrayQueue<String>();
		System.out.println(q);
		q.enqueue("A");
		System.out.println(q);
		q.enqueue("B");
		System.out.println(q);
		q.enqueue("C");
		System.out.println(q);
	}
}
