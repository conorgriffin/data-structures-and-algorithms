package com.conorgriffin.dsa.assignment4;

@SuppressWarnings("serial")
public class FullDequeException extends RuntimeException {
	public FullDequeException (String err) {
		super(err);
	}
}
