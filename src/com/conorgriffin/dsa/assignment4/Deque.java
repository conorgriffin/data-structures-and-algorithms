package com.conorgriffin.dsa.assignment4;

/**
 * A generic dequeue interface that provides 6 standard deque methods;
 * insertFirst(), insertLast(), removeFirst(), removeLast(), getFirst(), getLast()
 * 
 * @author conorgriffin
 *
 * @param <E> the type of object being stored
 */
public interface Deque<E> {
	
	/**
	 * Insert an element at the front of the deque
	 * @param element The element to be inserted
	 */
	public void insertFirst(E element);
	
	/**
	 * Insert an element at the rear of the deque
	 * @param element The element to be inserted
	 */
	public void insertLast(E element);
	
	/**
	 * Remove the element at the front of the deque
	 * @return the element to be removed
	 */
	public E removeFirst();
	
	/**
	 * Remove the element at the rear of the deque
	 * @return the element to be removed
	 */
	public E removeLast() throws EmptyDequeException;
	
	/**
	 * Get the element at the front of the deque
	 * @return the element to be returned
	 */
	public E getFirst() throws EmptyDequeException;
	
	/**
	 * Get the element at the rear of the deque
	 * @return the element to be returned
	 */
	public E getLast() throws EmptyDequeException;
	
	/**
	 * Get the size of the deque
	 * @return int size of the deque
	 */
	public int size();
	
	/**
	 * Check whether the deque is currently empty
	 */
	public boolean isEmpty();
}
