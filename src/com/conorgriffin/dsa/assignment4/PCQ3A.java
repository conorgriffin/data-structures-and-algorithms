package com.conorgriffin.dsa.assignment4;

public class PCQ3A {
	public static void main(String[] args) {
		ArrayQueue<String> a = new ArrayQueue<String>();
		
		System.out.println(a);
		a.enqueue("Ireland");
		System.out.println(a);
		a.dequeue();
		System.out.println(a);
		a.enqueue("England");
		System.out.println(a);
		a.dequeue();
		System.out.println(a);
		a.enqueue("Wales");
		System.out.println(a);
		a.dequeue();
		System.out.println(a);
		a.enqueue("Scotland");
		System.out.println(a);
		a.dequeue();
		System.out.println(a);
		a.enqueue("France");
		System.out.println(a);
		a.enqueue("Germany");
		System.out.println(a);		
	}
}
