package com.conorgriffin.dsa.assignment4;

@SuppressWarnings("serial")
public class FullQueueException extends RuntimeException {
	public FullQueueException (String err) {
		super(err);
	}
}
