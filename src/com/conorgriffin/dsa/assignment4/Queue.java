package com.conorgriffin.dsa.assignment4;

/**
 * A generic queue interface that provides 5 standard queue methods;
 * enqueue(), dequeue(), front(), isEmpty() and size()
 * 
 * @author conorgriffin
 *
 * @param <E> the type of object being stored
 */
public interface Queue<E> {
	/**
	 * Add an item to the back of the queue
	 * @param element the item to be added to the queue
	 */
	public void enqueue(E element);
	
	/**
	 * Fetch the item at the top of the queue and remove it from the queue
	 * @return the item at the top of the queue
	 * @throws EmptyQueueException
	 */
	public E dequeue() throws EmptyQueueException;
	
	/**
	 * Fetch the item at the top of the queue without removing it from the queue
	 * @return the item at the top of the queue
	 * @throws EmptyQueueException
	 */
	public E front() throws EmptyQueueException;
	
	/**
	 * Check to see if the queue is empty
	 * @return true if the queue is empty, otherwise return false
	 */
	public boolean isEmpty();
	
	/**
	 * The current size of the queue
	 * @return an integer representing the size of the queue
	 */
	public int size();
}
