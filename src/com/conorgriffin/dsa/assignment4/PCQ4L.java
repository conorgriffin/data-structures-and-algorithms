package com.conorgriffin.dsa.assignment4;

public class PCQ4L {
	public static void main(String[] args) {
		LinkedDeque<String> l = new LinkedDeque<>();
		
		System.out.println(l);
		l.insertFirst("Ireland");
		System.out.println(l);
		l.removeLast();
		System.out.println(l);
		l.insertLast("England");
		System.out.println(l);
		l.removeFirst();
		System.out.println(l);
		l.insertLast("Wales");
		System.out.println(l);
		l.insertFirst("Scotland");
		System.out.println(l);
		l.insertLast("France");
		System.out.println(l);
		l.removeFirst();
		System.out.println(l);
		l.removeLast();
		System.out.println(l);
		l.insertLast("Germany");
		System.out.println(l);
	}
}
